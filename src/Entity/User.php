<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Phonenumbers")
     */
    private $phonenumbers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address")
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Group")
     */
    private $roles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="friendsWithMe")
     */
    private $myFriends;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="myFriends")
     */
    private $friendsWithMe;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FieldUserTeam", mappedBy="user")
     */
    private $fieldUserTeams;

    public function __construct()
    {
        $this->phonenumbers = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->myFriends = new ArrayCollection();
        $this->friendsWithMe = new ArrayCollection();
        $this->fieldUserTeams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Phonenumbers[]
     */
    public function getPhonenumbers(): Collection
    {
        return $this->phonenumbers;
    }

    public function addPhonenumber(Phonenumbers $phonenumber): self
    {
        if (!$this->phonenumbers->contains($phonenumber)) {
            $this->phonenumbers[] = $phonenumber;
        }

        return $this;
    }

    public function removePhonenumber(Phonenumbers $phonenumber): self
    {
        if ($this->phonenumbers->contains($phonenumber)) {
            $this->phonenumbers->removeElement($phonenumber);
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(Group $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(Group $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getMyFriends(): Collection
    {
        return $this->myFriends;
    }

    public function addMyFriend(self $myFriend): self
    {
        if (!$this->myFriends->contains($myFriend)) {
            $this->myFriends[] = $myFriend;
        }

        return $this;
    }

    public function removeMyFriend(self $myFriend): self
    {
        if ($this->myFriends->contains($myFriend)) {
            $this->myFriends->removeElement($myFriend);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFriendsWithMe(): Collection
    {
        return $this->friendsWithMe;
    }

    public function addFriendsWithMe(self $friendsWithMe): self
    {
        if (!$this->friendsWithMe->contains($friendsWithMe)) {
            $this->friendsWithMe[] = $friendsWithMe;
            $friendsWithMe->addMyFriend($this);
        }

        return $this;
    }

    public function removeFriendsWithMe(self $friendsWithMe): self
    {
        if ($this->friendsWithMe->contains($friendsWithMe)) {
            $this->friendsWithMe->removeElement($friendsWithMe);
            $friendsWithMe->removeMyFriend($this);
        }

        return $this;
    }

    /**
     * @return Collection|FieldUserTeam[]
     */
    public function getFieldUserTeams(): Collection
    {
        return $this->fieldUserTeams;
    }

    public function addFieldUserTeam(FieldUserTeam $fieldUserTeam): self
    {
        if (!$this->fieldUserTeams->contains($fieldUserTeam)) {
            $this->fieldUserTeams[] = $fieldUserTeam;
            $fieldUserTeam->setUser($this);
        }

        return $this;
    }

    public function removeFieldUserTeam(FieldUserTeam $fieldUserTeam): self
    {
        if ($this->fieldUserTeams->contains($fieldUserTeam)) {
            $this->fieldUserTeams->removeElement($fieldUserTeam);
            // set the owning side to null (unless already changed)
            if ($fieldUserTeam->getUser() === $this) {
                $fieldUserTeam->setUser(null);
            }
        }

        return $this;
    }
}
