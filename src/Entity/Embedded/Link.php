<?php


namespace App\Entity\Embedded;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Link
 * @package App\Entity\Embedded
 * @ORM\Embeddable()
 */
class Link
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $link;
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $options;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options): void
    {
        $this->options = $options;
    }





}