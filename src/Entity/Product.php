<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Product
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
	
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	private $price;
	
	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Shipment", cascade={"persist", "remove"})
	 */
	private $shipment;
	
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Feature", mappedBy="product", orphanRemoval=true)
	 */
	private $features;

    /**
     * @ORM\Column(type="integer")
     */
    private $qty;
	
	public function __construct()
         	{
         		$this->features = new ArrayCollection();
         	}
	
	/**
	 * @return mixed
	 */
	public function getId()
         	{
         		return $this->id;
         	}
	
	
	/**
	 * @return mixed
	 */
	public function getName()
         	{
         		return $this->name;
         	}
	
	/**
	 * @param mixed $name
	 */
	public function setName($name): void
         	{
         		$this->name = $name;
         	}
	
	/**
	 * @return mixed
	 */
	public function getPrice()
         	{
         		return $this->price;
         	}
	
	/**
	 * @param mixed $price
	 */
	public function setPrice($price): void
         	{
         		$this->price = $price;
         	}
	
	public function getShipment(): ?Shipment
         	{
         		return $this->shipment;
         	}
	
	public function setShipment(Shipment $shipment): self
         	{
         		$this->shipment = $shipment;
         		
         		return $this;
         	}
	
	/**
	 * @return Collection|Feature[]
	 */
	public function getFeatures(): Collection
         	{
         		return $this->features;
         	}
	
	public function addFeature(Feature $feature): self
         	{
         		if (!$this->features->contains($feature)) {
         			$this->features[] = $feature;
         			$feature->setProduct($this);
         		}
         		
         		return $this;
         	}
	
	public function removeFeature(Feature $feature): self
         	{
         		if ($this->features->contains($feature)) {
         			$this->features->removeElement($feature);
         			// set the owning side to null (unless already changed)
         			if ($feature->getProduct() === $this) {
         				$feature->setProduct(null);
         			}
         		}
         		
         		return $this;
         	}

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }
}