<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Embedded\Link;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ManyToManyBidirectionalFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		for ($i = 0; $i < 4; $i++) {
			$tag = new Tag();
			$tag->setName('Tag '.$i);
			$manager->persist($tag);
		}
		$manager->flush();
		$tags = $manager->getRepository(Tag::class)->findAll();
		$langs = ["ru", "en"];
		for ($i = 0; $i < 20; $i++) {
			$article = new Article();
			$article->setTitle('Title '.$i);
			$lang = $langs[mt_rand(0, 1)];
			$article->setLangcode($lang);
			foreach ($tags as $tag){
				$article->addTag($tag);
			}

			$link = new Link();
			$link->setTitle("Some Title");
			$link->setLink("http://www.google.com");
			$link->setOptions([
			    'class' => 'some-class some-class-two',
                'id' => 'source_link'
            ]);
			$article->setStatus(mt_rand(0,1));
            $article->setSource($link);
			$manager->persist($article);
		}
		$manager->flush();
	}
	
}