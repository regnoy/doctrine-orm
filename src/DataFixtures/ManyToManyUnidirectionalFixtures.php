<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/10/2019
 * Time: 10:34 AM
 */

namespace App\DataFixtures;


use App\Entity\Group;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ManyToManyUnidirectionalFixtures extends Fixture implements DependentFixtureInterface
{
	public function getDependencies()
	{
		return [
				OneToManyUnidirectionalFixtures::class
		];
	}
	
	public function load(ObjectManager $manager)
	{
		$roles = ["ROLE_USER", "ROLE_ADMIN", "ROLE_MANAGER"];
		foreach ($roles as $item){
			$role = new Group();
			$role->setRole($item);
			$manager->persist($role);
		}
		$manager->flush();
		$groups = $manager->getRepository(Group::class)->findAll();
		$users = $manager->getRepository(User::class)->findAll();
		foreach ($users as $user){
			$role = $groups[mt_rand(0, 2)];
			$user->addRole($role);
		}
		$manager->flush();
	}
	
}