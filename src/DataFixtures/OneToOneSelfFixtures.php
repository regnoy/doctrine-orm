<?php

namespace App\DataFixtures;

use App\Entity\Student;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class OneToOneSelfFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		for($i=0; $i< 5 ; $i++){
			$student = new Student();
			$student->setName("Student ".$i);
			$manager->persist($student);
		}
		$manager->flush();
		$students = $manager->getRepository(Student::class)->findAll();
		for($i=0; $i< 3 ; $i++){
			$mentor = new Student();
			$mentor->setName("Mentor ".$i);
			$manager->persist($mentor);
			$student = $students[$i];
			$student->setMentor($mentor);
		}
		$manager->flush();
	}
	
}