<?php

namespace App\DataFixtures;

use App\Entity\Feature;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OneToManyBidirectionalFixtures extends Fixture implements DependentFixtureInterface
{
	public function getDependencies()
	{
		return[
				ProductFixtures::class
		];
	}
	
	public function load(ObjectManager $manager)
	{
		$products = $manager->getRepository(Product::class)->findAll();
		/** @var Product $product */
		foreach ($products as $product){
			for ($i=0; $i< 3; $i++){
				$feature = new Feature();
				$feature->setName("Feature:". $i. "Pr:".$product->getId());
				$manager->persist($feature);
				$product->addFeature($feature);
			}
		}
		$manager->flush();
	}
	
}