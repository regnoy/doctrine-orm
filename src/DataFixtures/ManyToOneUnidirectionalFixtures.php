<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/10/2019
 * Time: 10:19 AM
 */

namespace App\DataFixtures;


use App\Entity\Address;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ManyToOneUnidirectionalFixtures extends Fixture implements DependentFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		$lists = ["Stefan cel mare", "Alexandrul cel bun", "Mihai Viteazu"];
		foreach ($lists as $item){
			$address = new Address();
			$address->setName($item);
			$manager->persist($address);
		}
		$manager->flush();
		$address = $manager->getRepository(Address::class)->findAll();// 0, 1, 2
		$users = $manager->getRepository(User::class)->findAll();
		foreach ($users as $user){
			$rand = mt_rand(0, count($address) - 1 );// 1 = 0
			$add = $address[$rand];
			$user->setAddress($add);
		}
		$manager->flush();
	}
	
	public function getDependencies()
	{
		return[
				OneToManyUnidirectionalFixtures::class
		];
	}
	
}