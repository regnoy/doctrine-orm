<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ManyToManySelfFixtures extends Fixture implements DependentFixtureInterface
{
	public function getDependencies()
	{
		return [
				OneToManyUnidirectionalFixtures::class
		];
	}
	
	public function load(ObjectManager $manager)
	{
		$users = $manager->getRepository(User::class)->findAll();
		$ttl = count($users)- 1;// 1 - 20 = 0 - 19
		foreach ($users as $user){
			$friend1 = $users[mt_rand(0, $ttl)];
			$friend2 = $users[mt_rand(0, $ttl)];
			$friend3 = $users[mt_rand(0, $ttl)];
			$user->addMyFriend($friend1)->addMyFriend($friend2)->addMyFriend($friend3);
		}
		$manager->flush();
	}
	
}