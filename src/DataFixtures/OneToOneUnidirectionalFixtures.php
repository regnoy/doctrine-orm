<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/10/2019
 * Time: 7:59 AM
 */

namespace App\DataFixtures;


use App\Entity\Product;
use App\Entity\Shipment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OneToOneUnidirectionalFixtures extends Fixture implements DependentFixtureInterface
{
	public function getDependencies()
	{
		return[
				ProductFixtures::class,
				ShipmentFixtures::class
		];
	}
	
	public function load(ObjectManager $manager)
	{
		$productRepo = $manager->getRepository(Product::class);
		$shipmentRepo = $manager->getRepository(Shipment::class);
		$products = $productRepo->findAll();
		$shipments = $shipmentRepo->findAll();
		foreach ($products as $k => $product){
			$shipment = $shipments[$k];
			
			$product->setShipment($shipment);
		}
		$manager->flush();
	}
	
}