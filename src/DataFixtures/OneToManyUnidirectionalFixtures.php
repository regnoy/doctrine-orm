<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/10/2019
 * Time: 9:40 AM
 */

namespace App\DataFixtures;


use App\Entity\Phonenumbers;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class OneToManyUnidirectionalFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		$phones = ["+3736946541231", "+37369123123", "+37362342348"];
		$phoneNumbers= [];
		foreach ($phones as $phone){
			$phoneNumber = new Phonenumbers();
			$phoneNumber->setPhone($phone);
			$manager->persist($phoneNumber);
			$phoneNumbers[] = $phoneNumber;
		}
		$manager->flush();
		for($i =0 ; $i< 20; $i++){
			$user = new User();
			$user->setName("User ".$i);
			foreach ($phoneNumbers as $phoneNumber){
				$user->addPhonenumber($phoneNumber);
			}
			$manager->persist($user);
		}
		$manager->flush();
	}
	
}