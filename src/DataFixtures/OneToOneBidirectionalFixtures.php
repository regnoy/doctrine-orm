<?php

namespace App\DataFixtures;


use App\Entity\Cart;
use App\Entity\Customer;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OneToOneBidirectionalFixtures extends Fixture implements DependentFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		for($i=0; $i< 20 ; $i++){
			$customer = new Customer();
			$customer->setName('Customer: '.$i);
			$cart = new Cart();
			$customer->setCart($cart);
			$manager->persist($customer);
		}
		$manager->flush();
		$cart = $manager->getRepository(Cart::class)->findOneBy([]);
		$products = $manager->getRepository(Product::class)->findAll();
		foreach ($products as $product){
		    $cart->addProduct($product);
		}
		$manager->flush();
	}

    public function getDependencies()
    {
        return [
            ProductFixtures::class
        ];
    }

}