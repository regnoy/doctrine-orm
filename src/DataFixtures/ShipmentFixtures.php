<?php

namespace App\DataFixtures;

use App\Entity\Shipment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ShipmentFixtures extends Fixture implements DependentFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		for($i =0 ; $i< 20; $i++){
			$shipment = new Shipment();
			$shipment->setName("Shipment: ".$i);
			$shipment->setFullname("Fullname: ".$i);
			$manager->persist($shipment);
		}
		$manager->flush();
	}
	
	public function getDependencies()
	{
		return [
			ProductFixtures::class
		];
	}
	
}