<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class OneToManySelfFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		for ($i= 0 ; $i< 3; $i++) {
			$category = new Category();
			$category->setName("Children " . $i);
			$manager->persist($category);
		}
		$manager->flush();
		$childrens = $manager->getRepository(Category::class)->findAll();
		$category = new Category();
		$category->setName("Parent");
		$manager->persist($category);
		foreach ($childrens as $children){
			$category->addChildren($children);
		}
		$manager->flush();

	}
	
}