<?php


namespace App\DataFixtures;


use App\Entity\FieldUserTeam;
use App\Entity\Team;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TeamFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return[
            OneToManyBidirectionalFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();//20
        for($i= 0; $i< 5; $i++){
            $user = $users[$i];
            $team = new Team();
            $team->setName("Team". $i)->setUser($user);
            $manager->persist($team);
        }
        $manager->flush();
        $teams = $manager->getRepository(Team::class)->findAll();
        $teamOne = $teams[0];
        $teamtTwo = $teams[1];
        for($i= 5; $i< 10; $i++){
            $user = $users[$i];
            $fieldUserTeam = new FieldUserTeam();
            $fieldUserTeam->setUser($user)->setTeam($teamOne)->setConfirm(1);
            $fieldUserTeam->setCreated(new \DateTimeImmutable());
            $manager->persist($fieldUserTeam);
            $fieldUserTeam = new FieldUserTeam();
            $fieldUserTeam->setUser($user)->setTeam($teamtTwo)->setConfirm(1);
            $fieldUserTeam->setCreated(new \DateTimeImmutable());
            $manager->persist($fieldUserTeam);
        }
        $manager->flush();

    }

}