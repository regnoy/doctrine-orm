<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	for($i =0 ; $i< 20; $i++){
		    $product = new Product();
		    $product->setName("Product: ".$i);
		    $product->setPrice(mt_rand(1, 50));
		    $product->setQty(mt_rand(1, 10));
		    $manager->persist($product);
	    }
        $manager->flush();
    }
}
