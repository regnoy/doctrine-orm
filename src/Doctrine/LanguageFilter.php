<?php

namespace App\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class LanguageFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if($targetEntity->getName() != 'App\Entity\Article')
            return "";

        return sprintf("%s.langcode = %s",$targetTableAlias, $this->getParameter('langcode') );
    }

}