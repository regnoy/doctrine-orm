<?php

namespace App\Repository;

use App\Entity\Phonenumbers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Phonenumbers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Phonenumbers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Phonenumbers[]    findAll()
 * @method Phonenumbers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhonenumbersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Phonenumbers::class);
    }

    // /**
    //  * @return Phonenumbers[] Returns an array of Phonenumbers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Phonenumbers
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
