<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Product::class);
	}

	public function findPriceGreaterThen(int $price){
        $qry = $this->queryBuilder();
        $qry->andWhere('p.price > :price');
        $qry->setParameter(":price", $price);
        return $this->resultCollection($qry);
    }
    public function findLastProduct($limit){
        $qry = $this->queryBuilder();
        $qry->orderBy('p.id', "DESC");
        $qry->setMaxResults($limit);
        return $this->resultCollection($qry);
    }

    public function findCondition(){
        $qry = $this->queryBuilder();
//        $qry->andWhere('p.qty < :qty');
//        $qry->andWhere('p.qty <= :qty');
//        $qry->andWhere('p.qty > :qty');
//        $qry->andWhere('p.qty >= :qty');
//        $qry->andWhere("p.qty IN(:qty)");
        $qry->andWhere("p.qty between :start and :end");
        $qry->setParameters([
            ":start" => 4,
            ":end" => 8,
        ]);
        return $this->resultCollection($qry);
    }

    public function findWhereOrWhere(){
        $qry = $this->queryBuilder();
        $qry->andWhere('p.qty > 3 AND p.qty < 6');
        $qry = $this->findOrWhere($qry);
        $qry = $qry->orWhere("p.qty = 6");
        dump($qry->getDQL());
        return $this->resultCollection($qry);
    }
    public function findOrWhere(QueryBuilder $qry){
        return $qry->orWhere('p.qty > 7 AND p.qty < 10');
    }
    public function findLeftJoin(){
        $qry = $this->queryBuilder();
        $qry->leftJoin('p.shipment', 's');
        $qry->addSelect('s');
        return $this->resultCollection($qry);
    }
    public function findCountProduct(){
        $qry = $this->queryBuilder();
        $qry->select('COUNT(p.id)');
        //Conditions
        return $qry->getQuery()->getOneOrNullResult();
    }
    public function findTotalSumProduct(){
        $qry = $this->queryBuilder();
        $qry->select('SUM(p.price)');
        return $qry->getQuery()->getOneOrNullResult();
    }

    public function findSingleProduct(){
        $qry = $this->queryBuilder();
        $qry->setMaxResults(1);
        return $qry->getQuery()->getOneOrNullResult();
    }

    public function findDQL(){
        $qry = $this->getEntityManager()->createQuery("SELECT p, s FROM App\Entity\Product p LEFT JOIN p.shipment s");
        return $qry->getResult();
    }
    /**
     * @return QueryBuilder
     */
    private function queryBuilder(){
        return $this->createQueryBuilder('p');
    }
    /**
     * @param QueryBuilder $qry
     * @return ArrayCollection
     */
    private function resultCollection( QueryBuilder $qry){
        $results = $qry->getQuery()->getResult();
        $array = new ArrayCollection();
        foreach ($results as $result){
            $array->add($result);
        }
        return $array;
    }
}