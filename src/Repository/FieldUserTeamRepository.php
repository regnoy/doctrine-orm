<?php

namespace App\Repository;

use App\Entity\FieldUserTeam;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FieldUserTeam|null find($id, $lockMode = null, $lockVersion = null)
 * @method FieldUserTeam|null findOneBy(array $criteria, array $orderBy = null)
 * @method FieldUserTeam[]    findAll()
 * @method FieldUserTeam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldUserTeamRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FieldUserTeam::class);
    }

    // /**
    //  * @return FieldUserTeam[] Returns an array of FieldUserTeam objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FieldUserTeam
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
