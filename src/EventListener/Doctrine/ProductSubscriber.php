<?php


namespace App\EventListener\Doctrine;


use App\Entity\Product;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpKernel\KernelInterface;

class ProductSubscriber implements EventSubscriber
{
    private $kernel;
    public function __construct(KernelInterface $kernel)//custom
    {
        $this->kernel = $kernel;
    }

    public function preUpdate( LifecycleEventArgs $args ){
        dump($this->kernel->getProjectDir());
        $entity = $args->getEntity();
        if(!$entity instanceof Product)
            return;
        $entity->setQty(100);
        var_dump("PreUpdate Running:".  $entity->getQty());
    }
    public function postUpdate( LifecycleEventArgs $args ){
        $entity = $args->getEntity();
        if(!$entity instanceof Product)
            return;

        var_dump("PostUpdate Running".  $entity->getQty());
    }
    public function onFlush( OnFlushEventArgs $args ){

        $unitOfWork = $args->getEntityManager()->getUnitOfWork();
        var_dump("OnFLush");
        dump($unitOfWork);
    }
    public function postFlush( PostFlushEventArgs $args ){

        $unitOfWork = $args->getEntityManager()->getUnitOfWork();
        var_dump("postFlush");
        dump($unitOfWork);
    }
    public function getSubscribedEvents()
    {
        return [
            Events::preUpdate,
            Events::postUpdate,
            Events::onFlush,
            Events::postFlush
        ];
    }

}