<?php


namespace App\EventListener;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class LanguageFilterRequestListener
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onKernelRequest( RequestEvent $event ){
        $filters = $this->em->getFilters()->enable("language_filter");
        $filters->setParameter('langcode', 'en');
    }
}