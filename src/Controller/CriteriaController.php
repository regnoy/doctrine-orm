<?php


namespace App\Controller;


use App\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CriteriaController extends AbstractController
{

    /**
     * @Template("base.html.twig")
     * @Route(name="criteria", path="criteria")
     */
    public function criteria(){
        $tags = $this->getDoctrine()->getRepository(Tag::class)->findAll();
        /** @var Tag $tag */
        $tag = reset($tags);
//        dump($tag->getArticles()->toArray());
        dump($tag->getPublishedArticle());
    }
}