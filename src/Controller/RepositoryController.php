<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/8/2019
 * Time: 8:41 AM
 */

namespace App\Controller;


use App\Entity\Product;

use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RepositoryController extends AbstractController
{
	/**
	 * @Template("base.html.twig")
	 * @Route(path="/repository", name="repository")
	 */
	public function index(){
		$productRepo = $this->getDoctrine()->getRepository(Product::class);
		$products = $productRepo->findAll();
//		$product = $productRepo->find(3);
		$product = $productRepo->findOneBy(['price' => 22]);
		$productSecond = $productRepo->findBy(['price' => 22],['id' => 'DESC'], null, null);
		dump($product);
		dump($productSecond);
	}
    /**
     * @Template("base.html.twig")
     * @Route(path="query-builder", name="query_builder")
     */
    public function queryBuilder(){

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepo->findLastProduct(10);
        dump($products);
    }
    /**
     * @Template("base.html.twig")
     * @Route(path="query-builder-conditions", name="query_builder_conditions")
     */
    public function queryBuilderConditions(){

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepo->findCondition();
        dump($products);
    }
    /**
     * @Template("base.html.twig")
     * @Route(path="query-builder-where", name="query_builder_where")
     */
    public function queryBuilderWhere(){

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepo->findWhereOrWhere();
        dump($products);
    }
    /**
     * @Template("base.html.twig")
     * @Route(path="query-builder-left-join", name="query_builder_left-join")
     */
    public function queryBuilderLeftJoin(){

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepo->findLeftJoin();
        dump($products);
        /** @var Product $product */
        $product = $products->first();
        dump($product);
    }
    /**
     * @Template("base.html.twig")
     * @Route(path="query-builder-sum-count", name="query_builder_left-join")
     */
    public function queryBuilderSumCount(){

        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $totalProducts = $productRepo->findCountProduct();
        dump($totalProducts);
        $totalSums = $productRepo->findTotalSumProduct();
        dump($totalSums);
        try {
            $single = $productRepo->findSingleProduct();
            dump($single);
        } catch (NonUniqueResultException $e){
//            dump($e->getMessage());

        }
    }
    /**
     * @Template("base.html.twig")
     * @Route(path="query-builder-dql", name="query_builder_dql")
     */
    public function queryBuilderDQL(){
        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepo->findDQL();
        dump($products);
    }
}