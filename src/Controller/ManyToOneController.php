<?php

namespace App\Controller;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ManyToOneController extends AbstractController
{
	/**
	 * @Template("base.html.twig")
	 * @Route(path="many-to-one", name="many_to_one")
	 */
	public function ManyToOne(){
		$users = $this->getDoctrine()->getRepository(User::class)->findAll();
		/** @var User $user */
		$user = array_shift($users);
		dump($user->getAddress()->getName());
	}
}