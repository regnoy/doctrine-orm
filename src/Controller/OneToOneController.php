<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/10/2019
 * Time: 8:08 AM
 */

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\Student;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OneToOneController extends AbstractController
{
	
	/**
	 * @Template("base.html.twig")
	 * @Route(path="one-to-one-unidirectional", name="one_to_one_unidirectional")
	 */
	public function OneToOneUnidirectional(){
		$repo = $this->getDoctrine()->getRepository(Product::class);
		$produts = $repo->findAll();
		$product = $repo->find(22);
		dump($product);
		dump($produts);
	}
	/**
	 * @Template("base.html.twig")
	 * @Route(path="one-to-one-bidirectional", name="one_to_one_bidirectional")
	 */
	public function OneToOneBidirectional(){
		$customeRepo = $this->getDoctrine()->getRepository(Customer::class);
		$cartRepo = $this->getDoctrine()->getRepository(Cart::class);
		$cart = $cartRepo->findAll();
		$customer = $customeRepo->find(1);
		$customers = $customeRepo->findAll();
		dump($cart);
		dump($customer);
		dump($customers);
	}
	/**
	 * @Template("base.html.twig")
	 * @Route(path="one-to-one-self", name="one_to_one_self")
	 */
	public function OneToOneSelf(){
		$repo = $this->getDoctrine()->getRepository(Student::class);
		$student = $repo->find(1);
		$mentor = $repo->find(7);
		dump($student);
		dump($mentor);
		
	}
}