<?php


namespace App\Controller;


use App\Entity\Cart;
use App\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CollectionController extends AbstractController
{
    /**
     * @Template("base.html.twig")
     * @Route(path="/collection", name="collection")
     */
    public function collection()
    {
        $cart = $this->getDoctrine()->getRepository(Cart::class)->findOneBy([]);
        $products = $cart->getProducts();

//        $productFirst = $products->first();
//        $productLast = $products->last();
//        dump($productFirst);
//        dump($productLast);
        $count = $products->count();
        dump($count);
        dump($products);
//        dump($products->isEmpty());
//         $filter = $products->filter( function(Product $product){
//            return $product->getQty() > 5;
//         });
//         dump($filter);
//        $exists = $products->exists( function($key, Product $product){
//            return $product->getQty() == 11;
//        });
//        dump($exists);
//        $forAll = $filter->forAll( function($key, Product $product){
//            return $product->getQty() < 5;
//        });
//        dump($forAll);
//        $maps = $products->map( function(Product $product){
//            return $product->getQty() < 5;
//        });
//        dump($maps);
//        $slice = $products->slice(5, 5);
//        dump($slice);
    }
    /**
     * @Template("base.html.twig")
     * @Route(path="/lazy", name="lazy")
     */
    public function lazy(){
        $cart = $this->getDoctrine()->getRepository(Cart::class)->findOneBy([]);
        $products = $cart->getProducts();
        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy([]);


        $count = $products->count();
        $isEmpty = $products->isEmpty();
        dump($count);
        dump($isEmpty);
        dump($products->contains($product));
        $page = 2;
        dump($products->slice(10,10));

    }
}