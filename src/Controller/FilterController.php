<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FilterController extends AbstractController
{
    /**
     * @Route("/doctrine-filter", name="doctrine_filter")
     * @Template("base.html.twig")
     */
    public function doctrineFilter(){
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
//        $filters = $em->getFilters()->enable("language_filter");
//        $filters->setParameter('langcode', 'en');
        $products = $em->getRepository(Article::class)->findAll();
        dump($products);
    }
}