<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\FieldUserTeam;
use App\Entity\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ManyToManyController extends AbstractController
{
	/**
	 * @Template("base.html.twig")
	 * @Route(path="many-to-many-uni", name="many_many_uni")
	 */
	public function manyToManyUni(){
		$users = $this->getDoctrine()->getRepository(User::class)->findAll();
		dump($users[0]->getRoles()->toArray());
	}
	
	/**
	 * @Template("base.html.twig")
	 * @Route(name="many_to_many_bidirectional", path="many-to-many-bidirectional")
	 */
	public function manyToManyBidirectional(){
		$articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
		dump($articles);
		dump($articles[0]->getTags()->toArray());
		dump($articles[0]->getSource()->getTitle());//Link
	}
	/**
	 * @Template("base.html.twig")
	 * @Route(name="many_to_many_self", path="many-to-many-self")
	 */
	public function manyToManySelf(){
		$users = $this->getDoctrine()->getRepository(User::class)->findAll();
		/** @var User $user */
		foreach ($users as $user){
			dump($user->getId());
			dump($user->getMyFriends()->toArray());
			dump($user->getFriendsWithMe()->toArray());
			
		}
	}
    /**
     * @Template("base.html.twig")
     * @Route(name="many_to_many_alternative", path="many-to-many-alternative")
     */
    public function manyToManyAlternative(){
        $repo = $this->getDoctrine()->getRepository(FieldUserTeam::class);
        $field = $repo->findAll();
        /** @var FieldUserTeam $first */
        $first = reset($field);
        dump($first->getUser());

    }
}