<?php

namespace App\Controller;

use App\Entity\Product;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EntityController extends AbstractController
{
	/**
	 * @Template("base.html.twig")
	 * @Route(path="create/entity", name="create_entity")
	 */
	public function create(){
		
		$product = new Product();
		$product->setName("First Product");
		$product->setPrice(10);
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($product);
		$em->flush();
	}
	/**
	 * @Template("base.html.twig")
	 * @Route(path="update/entity", name="update_entity")
	 */
	public function update(){
		$em = $this->getDoctrine()->getManager();
		//Product::Class > App\Entity\Product;
		$productRepo = $em->getRepository(Product::class);
		/** @var Product $product */
		$product = $productRepo->find(2);
		$product->setPrice(33);
		
		$productTwo = $productRepo->find(3);
		$productTwo->setPrice(50);
		$em->flush($productTwo);
	}
    /**
     * @Template("base.html.twig")
     * @Route(path="remove/entity", name="remove_entity")
     */
    public function remove(){
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->findOneBy([]);
        $em->remove($product);
        $em->flush();
    }
}