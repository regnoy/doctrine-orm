<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 8/10/2019
 * Time: 9:17 AM
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OneToManyController extends AbstractController
{
	
	/**
	 * @Template("base.html.twig")
	 * @Route(path="one-to-many-bidirectional", name="one_many_bidi")
	 */
	public function OneToManyBidirectional(){
		$repoProd = $this->getDoctrine()->getRepository(Product::class);
		/** @var Product $product */
		$product = $repoProd->find(61);
//		foreach ($product->getFeatures() as $feature){
//			dump($feature);
//		}
		dump($product);
		dump($product->getFeatures()->toArray());
	}
	/**
	 * @Template("base.html.twig")
	 * @Route(path="one-to-many-uni", name="one_many_uni")
	 */
	public function OneToManyUni(){
		$users = $this->getDoctrine()->getRepository(User::class)->findAll();
		/** @var User $user */
		$user = array_shift($users);
		dump($user->getPhonenumbers()->toArray());
	}
	/**
	 * @Template("base.html.twig")
	 * @Route(path="one-to-many-self", name="one_many_self")
	 */
	public function OneToManySelf(){
		$repo = $this->getDoctrine()->getRepository(Category::class);
		$parent = $repo->find(1);
		dump($parent);
		dump($parent->getChildrens()->toArray());
		$children = $repo->find(7);
		dump($children);
		dump($children->getParent()->getName());
	}

    /**
     * @Template("base.html.twig")
     * @Route(name="orphan", path="orphan")
     */
    public function orphan(){
        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy([]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();
    }

    /**
     * @Template("base.html.twig")
     * @Route(name="doctrine-event", path="doctrine-event")
     */
    public function doctrineEvent(){
        $article = $this->getDoctrine()->getRepository(Article::class)->findOneBy([]);
        $article->setTitle('this is new Article');
        $em = $this->getDoctrine()->getManager();
        $em->flush();
    }

    /**
     * @Template("base.html.twig")
     * @Route(name="doctrine_event_service", path="doctrine-event-service")
     */
    public function doctrineEventService(){
        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy([]);
        $product->setName('this is Product'.mt_rand(0, 3));
        dump("start:". $product->getQty());
        $em = $this->getDoctrine()->getManager();
        $em->flush();
    }
}